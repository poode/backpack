<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'كلمة المرور يجيب ان تكون ستة أحرف على الأقل و مشابهة لتأكيد كلمة المرور',
    'reset'    => 'تم إعادة تعيين كلمة المرور!',
    'sent'     => 'تم ارسال وصلة تغير كلمة المرور عبر الايميل!',
    'token'    => 'وصلت تغير كلمة المرور غير سليمة',
    'user'     => "هذا البريد الالكنروني غير مسجل لدينا من قبل!",

];
